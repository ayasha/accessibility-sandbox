interface IFileIcon {
	width?: number;
	height?: number;
	sizeModifier?: number;
	viewBox?: string;
	fill?: string;
	className?: string;
}

/**
 * width and height to set the size directly
 * sizeModifier to modify width and height in aspect ratio
 *
 * @param param0
 * @returns
 */
export function FileIcon({
	width,
	height,
	sizeModifier = 1,
	viewBox,
	fill,
	className,
}: IFileIcon): JSX.Element {
	return (
		<svg
			xmlns="http://www.w3.org/2000/svg"
			width={width || 22 * sizeModifier}
			height={height || 28 * sizeModifier}
			viewBox={viewBox || '0 0 22 28'}
			fill='none'
			className={className}
		>
			<path
				d="M12.5262 9.99635H19.9512L12.5262 2.56409V9.99635ZM3.07622 0.537109H13.8762L21.9762 8.64503V24.8609C21.9762 25.5776 21.6918 26.2651 21.1854 26.7719C20.6791 27.2788 19.9923 27.5635 19.2762 27.5635H3.07622C2.36014 27.5635 1.67338 27.2788 1.16703 26.7719C0.660684 26.2651 0.376221 25.5776 0.376221 24.8609V3.23975C0.376221 1.73978 1.57772 0.537109 3.07622 0.537109ZM15.2262 22.1582V19.4556H3.07622V22.1582H15.2262ZM19.2762 16.7529V14.0503H3.07622V16.7529H19.2762Z"
				fill={fill || 'black'}
			/>
		</svg>
	);
}
