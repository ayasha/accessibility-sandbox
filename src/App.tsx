import { useState } from 'react'
import './App.css'
import { Box, Button, Dialog, Stack, Typography, styled } from '@mui/material'
import { FileIcon } from './icons/file-icon'

const StyledDialogPaperBox = styled(Box)`
  width: 40rem;
  height: 50rem;
  padding: 5rem 2rem;
  overflowX: hidden;
`;

const StyledHr = styled('hr')`
  width: 100%;
`;

function App() {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [isBlue, setIsBlue] = useState<boolean>(false);

  return (
    <>
    <Dialog  open={isOpen} onClose={() => setIsOpen(false)}>
      <StyledDialogPaperBox>
        <h2>Hello World</h2>
      </StyledDialogPaperBox>
    </Dialog>
      <Stack rowGap="5rem">
        <Typography component="h2">Eine Überschrift</Typography>
        <Stack rowGap="3rem">
          <Box>
            <Button onClick={() => setIsOpen(true)}>
              <FileIcon fill="white" />
            </Button>
          </Box>
          <StyledHr />
          <Box sx={{display: 'flex', columnGap: '2rem '}}>
            <Box sx={{width: '5rem', height: '5rem', backgroundColor: isBlue ? 'blue' : 'red'}} />
            <Button 
              onClick={() => setIsBlue(!isBlue)} 
              variant="outlined"
              disableRipple
            >
                <div>{isBlue ? 'In rot färben' : 'in blau färben'}</div>
            </Button>
          </Box>
          <StyledHr />
          <Box>
            <ul>
              <h3>Überschrift</h3>
              <li>Listen Punkt 1</li>
              <li>Listen Punkt 2</li>
              <li>Listen Punkt 3</li>
              <li>Listen Punkt 4</li>
            </ul>
          </Box>
        </Stack>
      </Stack>
    </>
  )
}

export default App
