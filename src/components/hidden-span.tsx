import { styled } from "@mui/material";

const StyledVisuallyHiddenSpan = styled('span')`
	clip: rect(1px, 1px, 1px, 1px);
	clip-path: inset(50%);
	height: 1px;
	width: 1px;
	margin: -1px;
	overflow: hidden;
	padding: 0;
	position: absolute;
	top: -800px;
	left: -800px;
`;

interface IVisuallyHiddenSpanProps {
	children: React.ReactNode | string;
	id?: string;
}

function HiddenSpan({ children, id, ...props }: IVisuallyHiddenSpanProps): JSX.Element {
	return (
		<StyledVisuallyHiddenSpan {...props} id={id}>
			{children}
		</StyledVisuallyHiddenSpan>
	);
}

export default HiddenSpan;
